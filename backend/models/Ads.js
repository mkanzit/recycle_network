const Sequelize = require('sequelize');
const Products = require('./Products');
const Users = require('./Users');
const db = require('../config/db.cfg');


// Define user model
const Ads = db.define('Ads', {
  title: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  city: {
    type: Sequelize.STRING
  },
  country: {
    type: Sequelize.STRING
  },
  product: {
    type: Sequelize.INTEGER
  },
  created: {
    type: Sequelize.DATE
  },
  updated: {
    type: Sequelize.DATE
  },
  deleted: {
    type: Sequelize.DATE
  },
}, {
  tableName: 'ads',
  timestamps: false,
  underscored: true,
  freezeTableName: true
});

Ads.belongsTo(Products, {
  as: 'ref_product',
  foreignKey: {
    name: 'product',
    allowNull: false
  }
});

Ads.belongsTo(Users, {
  as: 'ref_user',
  foreignKey: {
    name: 'user',
    allowNull: false
  }
});

// Products.belongsTo(Ads, {
//   foreignKey: {
//     name: 'id',
//     allowNull: false
//   }
// });

// Users.belongsTo(Ads, {
//   foreignKey: {
//     name: 'id',
//     allowNull: false
//   }
// });


module.exports = Ads;
