const Sequelize = require('sequelize');
const db = require('../config/db.cfg');

// Define product model
const Products = db.define('Products', {
  weight: {
    type: Sequelize.FLOAT
  },
  quantity: {
    type: Sequelize.INTEGER
  },
  images: {
    type: Sequelize.STRING
  },
  category: {
    type: Sequelize.INTEGER
  },
  ad: {
    type: Sequelize.INTEGER
  }
}, {
  tableName: 'products',
  timestamps: false,
  underscored: true,
  freezeTableName: true
});

module.exports = Products;
