const Sequelize = require('sequelize');
const db = require('../config/db.cfg');


// Define user model
const Users = db.define('Users', {
  fullname: {
    type: Sequelize.STRING
  },
  username: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  tel_1: {
    type: Sequelize.STRING
  },
  tel_2: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  postal_address: {
    type: Sequelize.STRING
  },
  city: {
    type: Sequelize.STRING
  },
  country: {
    type: Sequelize.STRING
  },
  image: {
    type: Sequelize.STRING
  },
  profile: {
    type: Sequelize.INTEGER
  },
  created: {
    type: Sequelize.DATE
  },
  updated: {
    type: Sequelize.DATE
  },
  deleted: {
    type: Sequelize.DATE
  },
}, {
  tableName: 'users',
  timestamps: false,
  underscored: true,
  freezeTableName: true
});


module.exports = Users;
