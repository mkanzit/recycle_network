// Dependencies
const path      = require('path');
const express   = require('express');
const cors      = require('cors');
const bodyParser = require('body-parser');
const passport   = require('passport');

const db = require('./config/db.cfg');

// Activate passport configuration
require('./config/passport.cfg');

const {
  accessRouter,
  usersRouter,
  adsRouter
} = require('./routes/index');

const { PORT }  = require('./config/net.cfg');


// Create the server
const server = express();

// Serve public / static files
server.use(express.static(path.join(__dirname, 'public')));
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(cors());
server.use(passport.initialize());

// Configure routes
server.use('/api/access', accessRouter);
server.use('/api/users', usersRouter);
server.use('/api/ads', adsRouter);


// Database connection
db
  .authenticate()
  .then(() => {
    console.log('DB Connected');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


// Start Express server on defined port
server.listen(PORT, () => {
  console.log(`Server start on port ${PORT}`);
});
