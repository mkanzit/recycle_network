const Sequelize = require('sequelize');

// Database configuration
const HOST = '127.0.0.1';
const DIALECT = 'mysql';
const USER = 'root';
const PWD  = '';
const DB   = 'recycle_network';


const sequelize = new Sequelize(DB, USER, PWD, {
  host: HOST,
  dialect: DIALECT,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

module.exports = sequelize;
