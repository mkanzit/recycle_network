// Network configuration
const PORT = process.env.PORT || 5000;

// Session secret
const SESSION_SECRET = process.env.SESSION_SECRET || 'sessionsecret';

module.exports = {
  PORT,
  SESSION_SECRET
};
