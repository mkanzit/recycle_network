const adsRoute = require('express').Router();
const Sequelize = require('sequelize');
const Ads = require('../models/Ads');
const Products = require('../models/Products');
const Users = require('../models/Users');

// Get All ads
adsRoute.get('/', (req, res) => {
  Ads.findAll({
    include: [{
      model: Products,
      as: 'ref_product'
    },
    {
      model: Users,
      as: 'ref_user'
    }]
  })
  .then( allAds => {
    res.json(allAds);
  })
  .catch( err => {
    console.log('DB Error', err);
    res.statusMessage = 'DB Error';
    res.sendStatus(500);
  });
});

// Get limits ads
adsRoute.get('/limit/:nbr', (req, res) => {
  const { nbr } = req.params;

  Ads.findAll({
    include: [{
      model: Products,
      as: 'ref_product'
    },
    {
      model: Users,
      as: 'ref_user'
    }],
    limit: parseInt(nbr, 10)
  })
  .then( limitAds => {
    res.json(limitAds);
  })
  .catch( err => {
    console.log('DB Error', err);
    res.statusMessage = 'DB Error';
    res.sendStatus(500);
  });
});

// Get one ads
adsRoute.get('/:id', (req, res) => {
  const { id } = req.params;

  Ads.findById(id, {
    include: [{
      model: Products,
      as: 'ref_product'
    },
    {
      model: Users,
      as: 'ref_user'
    }]
  })
  .then( oneAd => {
    res.json(oneAd);
  })
  .catch( err => {
    console.log('DB Error', err);
    res.statusMessage = 'DB Error';
    res.sendStatus(500);
  });
});

module.exports = adsRoute;
