const usersRouter = require('./users');
const accessRouter = require('./access');
const adsRouter = require('./ads');

module.exports = {
    accessRouter,
    usersRouter,
    adsRouter
};
