const access = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const { SESSION_SECRET } = require('../config/net.cfg');
const Users = require('../models/Users');


// Passport config
const authOptions = {
  successRedirect: '/',
  failureRedirect: '/access/login',
  session: false,
  failureFlash: 'Invalid username or password.',
  successFlash: 'Welcome!'
};

/**
 * @route /login
 * @desc Handle user login
 */
access.post('/login', (req, res) => {
  const { username, password } = req.body;

  // Passport authentication
  passport.authenticate( 'local', authOptions, (err, user, info) => {
    if (err || !user) {
      // User doesn't exist / Error
      res.statusMessage = info.message;
      return res.status(401).json({ message: info.message });
    }

    req.login( user, { session: false }, ( err ) => {
      if( err ) {
        // TODO: Add a custom logger
        console.log(err);
        return res.status(500).json({ message: "Internal error", err });
      }

      jwt.sign( user.dataValues, SESSION_SECRET, { expiresIn: '30m' }, (err, token) => {
        if( err ) {
          // TODO: Add a custom logger
          console.log(err);
          return res.status(500).json({ message: "Internal error" });
        }

        res.json({ token });
      });
    });
  })(req, res);
});

module.exports = access;
