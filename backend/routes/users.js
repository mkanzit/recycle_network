const users = require('express').Router();
const passport = require('passport');
const Users = require('../models/Users');

// Get user profile
users.get('/', (req, res) => {
  Users.findAll()
  .then( user => {
    res.json(user);
  })
  .catch( err => {
    console.log('DB Error', err);
    res.statusMessage = 'DB Error';
    res.sendStatus(500);
  });
});

// Get user profile
users.get('/:id', (req, res) => {
  const { id } = req.params;

  Users.findById(id)
  .then( users => {
    res.json(users);
  })
  .catch( err => {
    console.log('DB Error', err);
    res.statusMessage = 'DB Error';
    res.sendStatus(500);
  });
});

module.exports = users;
