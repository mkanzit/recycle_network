-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2018 at 05:43 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE `recycle_network`;

USE `recycle_network`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recycle_network`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` longtext,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `product` int(10) NOT NULL,
  `user` int(10) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `description`, `city`, `country`, `product`, `user`, `created`, `updated`, `deleted`) VALUES
(3, 'Carton', 'sdlgfhsdlghq sszdqhg qslgh lmqs ghq.', 'Casa', 'Maroc', 2, 2, '2018-09-19 10:32:22', NULL, NULL),
(4, 'Plastique', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Kenitra', 'Maroc', 3, 2, '2018-09-20 16:15:47', NULL, NULL),
(5, 'Aluminium', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in', 'Tanger', 'Maroc', 4, 3, '2018-09-20 16:15:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `weight` float DEFAULT NULL,
  `quantity` int(15) DEFAULT NULL,
  `images` varchar(800) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `ad` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `weight`, `quantity`, `images`, `category`, `ad`) VALUES
(2, 3000, 120, 'recycle-ad01.jpg;recycle-ad04.jpg;recycle-ad05.jpg', 'Carton', 3),
(3, 200, 3000, 'recycle-ad02.jpg', 'Plastique', 4),
(4, 250, NULL, 'recycle-ad03.jpg', 'Aluminium', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `tel_1` varchar(12) DEFAULT NULL,
  `tel_2` varchar(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `postal_address` varchar(150) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `profile` int(10) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `password`, `tel_1`, `tel_2`, `email`, `postal_address`, `city`, `country`, `image`, `profile`, `created`, `updated`, `deleted`) VALUES
(2, 'Elachkoura Abdelfattah', 'abela', '123456', '0633401861', '0688111217', 'elachkoura@gmail.com', '277 Oued Eddahab jamila 5 c.d', 'Casablanca', 'Maroc', 'profile.jpg', 1, '2018-09-19 09:12:46', NULL, NULL),
(3, 'Wahiba Zina', 'wiba', '123456', '036598741', NULL, 'wiba@mail.com', '12 bd. zohor', 'Rabat', 'Maroc', 'user02.jpg', 2, '2018-09-20 16:23:04', NULL, NULL),
(4, 'Anass Larson', 'anlson', '123456', '0655789412', NULL, 'anlson@email.org', '45 a.v. ouhod 22546', 'Marrakech', 'Maroc', 'user03.jpg', 3, '2018-09-20 16:23:04', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
