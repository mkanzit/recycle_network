## RECYCLE NETWORK PROJECT USING NODE.JS, MYSQL & ANGULAR 6

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

**if you want to edit / create files you should follow instructions below**
### Prerequisites

What things you need to install the software and how to install them.

##### 1 - Install Node JS using the link below:
```
 https://www.npmjs.com/get-npm
```

### Installing

A step by step series of examples that tell you have to get a development env running

##### 1 - Clone repository using:

```
git clone  https://github.com/Elachkoura/recycle_network.git.
```

##### 2 - Go to directory path:

```
cd recycle_network
```

##### 3 - Install dependencies using:

```
npm install
```

##### 4 - Init Project:
* You have to fill some informations using the commande below:
    * it will give you few questions , you juste have to fill them and ENJOY !


##### 5 - Run / Serve API using:

```
npm start
```

##### 6 - Run / Serve App ANGULAR using:

```
cd client/
npm start
```

## Authors

* **Abdelfattah Elachkoura**  - *FrontEnd Dev* - [AbdelfattahElachkoura](https://linkedin.com/in/elachkouraabdel)
