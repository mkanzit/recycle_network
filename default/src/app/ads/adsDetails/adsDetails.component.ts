import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';


import { AdsService } from '../../services/ads.service';
import { environment } from '../../../environments/environment';
import { tileLayer, latLng } from 'leaflet';

@Component({
  selector: 'ads-details-cmp',
  templateUrl: 'adsDetails.component.html',
})

export class AdsDetailsComponent implements OnInit {

  adsDetails: any;
  adId: number;
  title: string = environment.site.title;

  mapOptions: any = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18 })
    ],
    zoom: 12,
    center: latLng(33.573109, -7.589843)
  };

  constructor(
    private route: ActivatedRoute,
    private adsService: AdsService,
    private titleService: Title
  ) {
    window.scrollTo(0, 0);
    this.adId = parseInt(this.route.snapshot.params.id, 10);
  }

  ngOnInit() {
    this.adsService.getAdsInfos(this.adId)
      .then(resp => {
        this.adsDetails = resp;
        if (resp) {
          this.titleService.setTitle(this.title.concat(' - ').concat('Annonce > ' + resp.title));
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
}
