import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

/**
 * Network Layer:
 * @description All network stuff ( Requests / Errors )
 * are managed down here
 */

@Injectable()
export class UsersService {


  constructor( public http: HttpClient ) { }


  /**
   * @description Get user infos.
   * @param id
   *
   * @return Promise<any>
   */
  public getUserInfos(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.baseUrl + '/users/' + id)
      .subscribe((res: any) => {
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

  /**
   * @description Get all users.
   * @param none
   *
   * @return Promise<any>
   */
  public getUsers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.baseUrl + '/users')
      .subscribe((res: any) => {
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

  /**
   * @description login.
   * @param none
   *
   * @return Promise<any>
   */
  public logIn(payload): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(environment.baseUrl + '/access/login', payload)
      .subscribe((res: any) => {
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

}
