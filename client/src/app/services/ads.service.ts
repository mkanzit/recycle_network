import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

/**
 * Network Layer:
 * @description All network stuff ( Requests / Errors )
 * are managed down here
 */

@Injectable()
export class AdsService {


  constructor( public http: HttpClient ) { }


  /**
   * @description Get ads infos.
   * @param id
   *
   * @return Promise<any>
   */
  public getAdsInfos(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.baseUrl + '/ads/' + id)
      .subscribe((res: any) => {
        const imgs = res.ref_product.images.split(';');
        res.imgs = imgs;
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

  /**
   * @description Get all ads.
   * @param none
   *
   * @return Promise<any>
   */
  public getAds(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.baseUrl + '/ads')
      .subscribe((res: any) => {
        res.forEach(item => {
          const imgs = item.ref_product.images.split(';');
          item.imgs = imgs;
        });
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

  /**
   * @description Get limit ads.
   * @param nbr
   *
   * @return Promise<any>
   */
  public getAdsLimit(nbr: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.baseUrl + '/ads/limit/' + nbr)
      .subscribe((res: any) => {
        res.forEach(item => {
          const imgs = item.ref_product.images.split(';');
          item.imgs = imgs;
        });
        resolve(res);
      }, (error: any) => {
        reject(error);
      });
    });
  }

}
