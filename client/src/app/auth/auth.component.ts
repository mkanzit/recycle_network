import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { UsersService } from '../services/user.service';
import { environment } from '../../environments/environment';
import { CookiesStorageService, LocalStorageService, SessionStorageService, SharedStorageService } from 'ngx-store';
import { Router } from '@angular/router';
import { EventsService } from 'angular4-events';

@Component({
  selector: 'auth-cmp',
  templateUrl: 'auth.component.html'
})

export class AuthComponent implements OnInit {
  viewLoginForm = false;
  title: string = environment.site.title;

  loginForm = new FormGroup({
    username : new FormControl(null, [
      Validators.required
    ]),
    password : new FormControl(null, [
      Validators.required
    ])
  });
  submitted = false;

  constructor (
    private route: ActivatedRoute,
    private usersService: UsersService,
    private titleService: Title,
    private formBuilder: FormBuilder,
    private storage: LocalStorageService,
    private router: Router,
    private events: EventsService
  ) {
    // On User logIn we subscribe to event to refrensh content.
    this.events.subscribe('loggedIn').subscribe(data => {
      this.checkIfLoggedIn();
    });

    this.checkIfLoggedIn();
  }

  public checkIfLoggedIn () {
    const token = this.storage.get('myToken');
    if (token) {
      this.viewLoginForm = true;
    } else {
     this.viewLoginForm = false;
    }
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
  
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    const payload = {username: null, password: null};

    payload.username = this.loginForm.get('username').value;
    payload.password = this.loginForm.get('password').value;

    this.usersService.logIn(payload).then(token => {
      // Store Token.
      this.storage.set('myToken', token);
      if (token) {
        window.location.href = "/index";
      } else {
        this.router.navigate(['/auth']);
      }
    }).catch(error => {
      console.log('error login');
    });
  }
}
