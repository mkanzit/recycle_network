import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { AdsService } from '../services/ads.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'ads-cmp',
  templateUrl: 'ads.component.html'
})

export class AdsComponent implements OnInit {

  ads: any;
  title: string = environment.site.title;

  constructor (
    private route: ActivatedRoute,
    private adsService: AdsService,
    private titleService: Title
  ) {

  }

  ngOnInit() {
    this.adsService.getAds()
      .then(resp => {
        this.ads = resp;

        if (resp) {
          this.titleService.setTitle(this.title.concat(' - ').concat('Annonces'));
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
}
