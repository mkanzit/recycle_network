import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/user.service';

@Component({
  selector: 'user-cmp',
  templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {

  users: any;
  userInfos: any;

  constructor(private usersService: UsersService) {

  }

  ngOnInit() {
    // this.storage.get('user').then(data => {
    //     if(data) {
    //         this.usersService.getUserInfos(data.id)
    //         .then( resp => {
    //             this.userInfos = resp[0];
    //         })
    //         .catch( err => {
    //             console.log(err);
    //         });

    //         this.usersService.getUsers()
    //         .then( resp => {
    //             console.log(resp);
    //             this.users = resp;
    //         })
    //         .catch( err => {
    //             console.log(err);
    //         });
    //     }
    // });

    this.usersService.getUserInfos(2)
      .then(resp => {
        this.userInfos = resp;
      })
      .catch(err => {
        console.log(err);
      });

    this.usersService.getUsers()
      .then(resp => {
        console.log(resp);
        this.users = resp;
      })
      .catch(err => {
        console.log(err);
      });
  }
}
